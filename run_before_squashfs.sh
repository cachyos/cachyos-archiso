#!/bin/bash

# Made by Fernando "maroto"
# Run anything in the filesystem right before being "mksquashed"
# ISO-NEXT specific cleanup removals and additions (08-2021) @killajoe and @manuel

script_path=$(readlink -f ${0%/*})
work_dir=work

# Adapted from AIS. An excellent bit of code!
arch_chroot(){
    arch-chroot $script_path/${work_dir}/x86_64/airootfs /bin/bash -c "${1}"
}

do_merge(){

arch_chroot "

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen
ln -sf /usr/share/zoneinfo/UTC /etc/localtime
usermod -s /usr/bin/bash root
cp -aT /etc/skel/ /root/
rm /root/xed.dconf
chmod 700 /root
useradd -m -p \"\" -g users -G 'sys,rfkill,wheel' -s /bin/bash cachyos
git clone --single-branch https://gitlab.com/cachyos/liveuser-desktop-settings.git
#git clone https://gitlab.com/cachyos/liveuser-desktop-settings.git
cd liveuser-desktop-settings
rm -R /home/cachyos/.config
cp -R .config /home/cachyos/
chown -R cachyos:cachyos /home/cachyos/.config
cp .xinitrc .xprofile .Xauthority /home/cachyos/
chown cachyos:cachyos /home/cachyos/.xinitrc
chown cachyos:cachyos /home/cachyos/.xprofile
chown cachyos:cachyos /home/cachyos/.Xauthority
cp -R .local /home/cachyos/
chown -R cachyos:cachyos /home/cachyos/.local
chmod +x /home/cachyos/.local/bin/*
cp user_pkglist.txt /home/cachyos/
chown cachyos:cachyos /home/cachyos/user_pkglist.txt
rm /home/cachyos/.bashrc
cp .bashrc /home/cachyos/
chown cachyos:cachyos /home/cachyos/.bashrc
cp LICENSE /home/cachyos/
cd ..
rm -R liveuser-desktop-settings
chmod 755 /etc/sudoers.d
mkdir -p /media
chmod 755 /media
chmod 440 /etc/sudoers.d/g_wheel
chown 0 /etc/sudoers.d
chown 0 /etc/sudoers.d/g_wheel
chown root:root /etc/sudoers.d
chown root:root /etc/sudoers.d/g_wheel
chmod 755 /etc
sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
# sed -i 's/#Server/Server/g' /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf
sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf
systemctl enable NetworkManager.service vboxservice.service vmtoolsd.service vmware-vmblock-fuse.service systemd-timesyncd
systemctl set-default multi-user.target

cp -rf /usr/share/mkinitcpio/hook.preset /etc/mkinitcpio.d/linux.preset
sed -i 's?%PKGBASE%?linux?' /etc/mkinitcpio.d/linux.preset

# fetch fallback mirrorlist for offline installs:
wget https://gitlab.com/cachyos/cachyos-archiso/-/raw/master/mirrorlist
cp mirrorlist /etc/pacman.d/
rm mirrorlist

# now done with recreating pacman keyring inside calamares:
# shellprocess_initialize_pacman
#pacman-key --init
#pacman-key --add /usr/share/pacman/keyrings/cachyos.gpg && sudo pacman-key --lsign-key F3B607488DB35A47
#pacman-key --populate
#pacman-key --refresh-keys
#pacman -Syy

# to install locally builded packages on ISO:
#pacman -U --noconfirm /root/cachyos-hello-0.6.7-1-x86_64.pkg.tar.zst
#rm /root/cachyos-hello-0.6.7-1-x86_64.pkg.tar.zst
#rm /var/log/pacman.log

# now done with recreating pacman keyring inside calamares:
# shellprocess_initialize_pacman
#rm -R /etc/pacman.d/gnupg

sed -i 's|^GRUB_CMDLINE_LINUX_DEFAULT=\"\(.*\)\"$|GRUB_CMDLINE_LINUX_DEFAULT=\"\1 nowatchdog\"|' /etc/default/grub
sed -i 's?GRUB_DISTRIBUTOR=.*?GRUB_DISTRIBUTOR=\"CachyOS\"?' /etc/default/grub
sed -i 's?\#GRUB_THEME=.*?GRUB_THEME=\/boot\/grub\/themes\/CachyOS\/theme.txt?g' /etc/default/grub
sed -i 's?\#GRUB_DISABLE_SUBMENU=y?GRUB_DISABLE_SUBMENU=y?g' /etc/default/grub
echo 'GRUB_DISABLE_OS_PROBER=false' >> /etc/default/grub
# rm /boot/grub/grub.cfg --> seems mkarchiso is doing this now?

wget https://gitlab.com/cachyos/liveuser-desktop-settings/-/raw/master/dconf/xed.dconf
dbus-launch dconf load / < xed.dconf
sudo -H -u cachyos bash -c 'dbus-launch dconf load / < xed.dconf'
rm xed.dconf
chmod -R 700 /root
chown root:root -R /root
chown root:root -R /etc/skel
mkdir -p /root/src
ln -s /usr/share/calamares/qml /root/src/qml
touch /CHANGES
chsh -s /bin/bash"
}

#################################
########## STARTS HERE ##########
#################################

do_merge
